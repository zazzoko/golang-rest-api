package Controllers

import (
	"golangapi/app/Models"
	"golangapi/app/Services"
	"golangapi/app/Database"

	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetAllUsers(c *gin.Context) {
	var user []Models.User
	err := Services.GetAllUsers(&user)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, user)
	}
}

func CreateUser(c *gin.Context) {
	var user Models.User
	c.BindJSON(&user)
	err := Services.CreateUser(&user)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": "Email not found!"})
	} else {
		c.JSON(http.StatusOK, user)
	}
}

func GetUserByEmail(c *gin.Context) {
	email := c.Param("email")
	var user Models.User
	err := Services.GetUserByEmail(&user, email)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Email not found!"})
	} else {
		c.JSON(http.StatusOK, user)
	}
}

func DeleteUserByEmail(c *gin.Context) {
	var user Models.User
	email := c.Param("email")
	err := Services.DeleteUserByEmail(&user, email)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, gin.H{"Email: " + email: "is deleted"})
	}
}


func UpdateUserByEmail(c *gin.Context) {
	var user Models.User
	if err := Database.Connector.Where("email = ?", c.Param("email")).First(&user).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Email not found!"})
	  return
	}
  
	// Validate input
	var input Models.User
	if err := c.ShouldBindJSON(&input); err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	  return
	}
  
	Database.Connector.Model(&user).Updates(input)
  
	c.JSON(http.StatusOK, gin.H{"data": user})
  }