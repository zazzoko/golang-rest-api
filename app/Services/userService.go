
package Services

import (
	"golangapi/app/Database"
	"golangapi/app/Models"

	_ "github.com/go-sql-driver/mysql"
)

func GetAllUsers(user *[]Models.User) (err error) {
	if err = Database.Connector.Find(user).Error; err != nil {
		return err
	}
	return nil
}

func CreateUser(user *Models.User) (err error) {
	if err = Database.Connector.Create(user).Error; err != nil {
		return err
	}
	return nil
}

func GetUserByEmail(user *Models.User, email string) (err error) {
	if err = Database.Connector.Where("email = ?", email).First(user).Error; err != nil {
		return err
	}
	return nil
}

func DeleteUserByEmail(user *Models.User, email string) (err error) {
	Database.Connector.Where("email = ?", email).Delete(user)
	return nil
}