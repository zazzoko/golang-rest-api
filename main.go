package main

import (
	"golangapi/app/Database"
	"golangapi/app/Routes"

)

var err error

func main() {
	Database.InitDB()
	r := Routes.SetupRouter()
	//running
	r.Run(":9000")
}
